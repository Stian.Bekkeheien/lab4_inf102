package INF102.lab4.median;

import java.util.ArrayList;
import java.util.List;

public class QuickMedian implements IMedian {

    @Override
    public <T extends Comparable<T>> T median(List<T> list) {
        List<T> listCopy = new ArrayList<>(list); // Method should not alter list
        int s = listCopy.size();
        return choose(listCopy, 0, s - 1 , s / 2);
    }
private <T extends Comparable<T>> T choose(List<T> listCopy, int i, int j, int e){
    if (i == j) {
        return listCopy.get(i);
    }
    int changein = division(listCopy, i , j);
    if (e == changein) {
        return listCopy.get(e); 
    } 
    else if (e < changein) {
        return choose(listCopy, i, changein -1, e);
    }
    else {
        return choose(listCopy, changein + 1, j, e);
    }

}
private <T extends Comparable<T>> int division(List<T> listcopy, int i, int j) {
    T pivot = listcopy.get(i);
    int r = j;
    int l = i + 1;
    while (l <= r){
        
        while (l <= r && listcopy.get(l).compareTo(pivot) <= 0){
            l++;
        }
        while (l<= r && listcopy.get(r).compareTo(pivot) > 0){
            r--;
        }
        if (l < r){
            change(listcopy, l, r);
        }
    }
    change(listcopy, i, r);
    return r;
}
private <T> void change(List<T> listCopy, int l, int r){
    T tempLeft = listCopy.get(l);
    T tempRight = listCopy.get(r);
    listCopy.set(l, tempRight);
    listCopy.set(r, tempLeft);

}

}
