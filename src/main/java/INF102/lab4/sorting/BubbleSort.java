package INF102.lab4.sorting;

import java.util.List;

public class BubbleSort implements ISort {

    @Override
    public <T extends Comparable<T>> void sort(List<T> list) {
        int n = list.size();

        for (int i = 0; i < n - 1; i++)
            for (int j = 0; j < n - 1 - i; j++)
             if (list.get(j).compareTo(list.get(j+1)) > 0){
                 T one = list.get(j);
                 T two = list.get(j+1);
                 list.set(j, two);
                 list.set(j + 1, one);
                
             }
             
        return;
    }

}
    

